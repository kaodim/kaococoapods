Pod::Spec.new do |s|
  s.name             = 'KaoDesignIos'
  s.version          = '0.1.6'
  s.summary          = 'Kaodim custom design library'
 
  s.description      = <<-DESC
KaoDesignIos provide custom ui 
                       DESC
 
  s.homepage         = 'https://auyotoc@bitbucket.org/kaodim/kaodesignios.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Augustius' => 'tech+ios@kaodim.com' }
  s.source           = { :git => 'https://auyotoc@bitbucket.org/kaodim/kaodesignios.git', :tag => s.version.to_s }

  s.source_files = 'Sources/**/*.{swift}'
  s.resource_bundles = {
    'KaoCustomPod' => [
        'Sources/**/*.xib',
        'Sources/Resources/Fonts/*.ttf',
        'Sources/Resources/icons.xcassets'
    ]
  }

  s.pod_target_xcconfig = {
     "SWIFT_VERSION" => "4.0",
  }
  s.ios.deployment_target = '9.0'
  s.requires_arc = true
 
end