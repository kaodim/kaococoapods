## Push your private repo Pods to this repo

pod repo push https://auyotoc@bitbucket.org/kaodim/kaococoapods.git --swift-version="4.0"

OR

pod repo push git@bitbucket.org:kaodim/kaococoapods.git --swift-version="4.0"

## Kao Otp Flow 
1. Do changes in Pods > Development Pods > KaoOtpFlow
2. Commit changes
3. Update pod spec file - **KaoOtpFlow.podspec**, bump the version and then commit
4. Go to [https://bitbucket.org/kaodim/kao-ios-otp/commits/all](https://bitbucket.org/kaodim/kao-ios-otp/commits/all) and add tag by going to specific commit
5. To validate **pod lib lint –swift-version=“4.0”**
6. To push -> pod repo push kaococoapods –swift-version=“4.0” –verbose –allow-warnings
7. In the kaodim project, run **pod update** **KaoOtpFlow**